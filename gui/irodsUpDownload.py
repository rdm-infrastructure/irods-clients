from PyQt5.QtWidgets import QMainWindow, QHeaderView, QMessageBox, QWidget
from PyQt5 import QtCore
import os

from utils.utils import getSize, saveIenv
from gui.checkableFsTree import checkableFsTreeModel
from gui.irodsTreeView  import IrodsModel
from gui.continousUpload import contUpload
from gui.popupWidgets import irodsCreateCollection, createDirectory
from gui.dataTransfer import dataTransfer
from gui.ui_files.tabUpDownload import Ui_tabUpDownload

class irodsUpDownload(QWidget, Ui_tabUpDownload):
    def __init__(self, ic, ienv):
        super(irodsUpDownload, self).__init__()
        super(irodsUpDownload, self).setupUi(self)

        self.ic = ic
        self.ienv = ienv
        self.syncing = False # syncing or not

        rescs = self.ic.listResources()

        # QTreeViews
        self.dirmodel = checkableFsTreeModel(self.localFsTreeView)
        self.localFsTreeView.setModel(self.dirmodel)
        # Hide all columns except the Name
        self.localFsTreeView.setColumnHidden(1, True)
        self.localFsTreeView.setColumnHidden(2, True)
        self.localFsTreeView.setColumnHidden(3, True)
        self.localFsTreeView.header().setSectionResizeMode(QHeaderView.ResizeToContents)
        home_location = QtCore.QStandardPaths.standardLocations(
                               QtCore.QStandardPaths.HomeLocation)[0]
        index = self.dirmodel.setRootPath(home_location)
        self.localFsTreeView.setCurrentIndex(index)
        self.dirmodel.initial_expand()
        
        # iRODS  zone info
        self.irodsZoneLabel.setText("/"+self.ic.session.zone+":")
        # iRODS tree
        self.irodsmodel = IrodsModel(ic, self.irodsFsTreeView)
        self.irodsFsTreeView.setModel(self.irodsmodel)
        self.irodsRootColl = '/'+ic.session.zone
        self.irodsmodel.setHorizontalHeaderLabels([self.irodsRootColl,
                                              'Level', 'iRODS ID',
                                              'parent ID', 'type'])

        self.irodsFsTreeView.expanded.connect(self.irodsmodel.refreshSubTree)
        self.irodsFsTreeView.clicked.connect(self.irodsmodel.refreshSubTree)
        self.irodsmodel.initTree()

        self.irodsFsTreeView.setHeaderHidden(True)
        self.irodsFsTreeView.header().setDefaultSectionSize(180)
        self.irodsFsTreeView.setColumnHidden(1, True)
        self.irodsFsTreeView.setColumnHidden(2, True)
        self.irodsFsTreeView.setColumnHidden(3, True)
        self.irodsFsTreeView.setColumnHidden(4, True)

        # Buttons
        self.UploadButton.clicked.connect(self.upload)
        self.DownloadButton.clicked.connect(self.download)
        #self.ContUplBut.clicked.connect(self.cont_upload)
        self.ContUplBut.setHidden(True) #fFor first release hide special buttons
        self.uplSetGB_2.setHidden(True)
        self.createFolderButton.clicked.connect(self.createFolder)
        self.createCollButton.clicked.connect(self.createCollection)

        # Resource selector
        available_resources = self.ic.listResources()
        self.resourceBox.clear()
        self.resourceBox.addItems(available_resources)
        if ("default_resource_name" in ienv) and \
                (ienv["default_resource_name"] != "") and \
                (ienv["default_resource_name"] in available_resources):
            index = self.resourceBox.findText(ienv["default_resource_name"])
            self.resourceBox.setCurrentIndex(index)

        # Continious upload settings
        if ienv["irods_host"] in ["scomp1461.wur.nl", "npec-icat.irods.surfsara.nl"]:
            #self.uplSetGB_2.setVisible(True)
            if ("ui_remLocalcopy" in ienv):
                self.rLocalcopyCB.setChecked(ienv["ui_remLocalcopy"])
            if ("ui_uplMode" in ienv):
                uplMode =  ienv["ui_uplMode"]
                if uplMode == "f500":
                    self.uplF500RB.setChecked(True)
                elif uplMode == "meta":
                    self.uplMetaRB.setChecked(True)
                else:
                    self.uplAllRB.setChecked(True)
            #self.rLocalcopyCB.stateChanged.connect(self.saveUIset)
            #self.uplF500RB.toggled.connect(self.saveUIset)
            #self.uplMetaRB.toggled.connect(self.saveUIset)
            #self.uplAllRB.toggled.connect(self.saveUIset)
        else:
            self.uplSetGB_2.hide()
            self.ContUplBut.hide()


    def enableButtons(self, enable):
        self.UploadButton.setEnabled(enable)
        self.DownloadButton.setEnabled(enable)
        self.ContUplBut.setEnabled(enable)
        self.uplSetGB_2.setEnabled(enable)
        self.createFolderButton.setEnabled(enable)
        self.createCollButton.setEnabled(enable)
        self.localFsTreeView.setEnabled(enable)
        self.irodsFsTreeView.setEnabled(enable)


    def infoPopup(self, message):
        QMessageBox.information(self.widget, 'Information', message)


    def saveUIset(self):
        self.ienv["ui_remLocalcopy"] = self.getRemLocalCopy()
        self.ienv["ui_uplMode"] = self.getUplMode()
        saveIenv(self.ienv)


    def getResource(self):
        return self.resourceBox.currentText()

    def getRemLocalCopy(self):
        return self.rLocalcopyCB.isChecked()

    def getUplMode(self):
        if self.uplF500RB.isChecked():
            uplMode = "f500"
        elif self.uplMetaRB.isChecked():
            uplMode = "meta"
        else: # Default
            uplMode = "all"
        return uplMode


    def createFolder(self):
        parent = self.dirmodel.get_checked()
        if parent == None:
            self.errorLabel.setText("No parent folder selected.")
        else:
            createDirWidget = createDirectory(parent)
            createDirWidget.exec_()
            #self.dirmodel.initial_expand(previous_item = parent)


    def createCollection(self):
        idx, parent = self.irodsmodel.get_checked()
        if parent == None:
            self.errorLabel.setText("No parent collection selected.")
        else:
            creteCollWidget = irodsCreateCollection(parent, self.ic)
            creteCollWidget.exec_()
            self.irodsmodel.refreshSubTree(idx)


    # Upload a file/folder to IRODS and refresh the TreeView
    def upload(self):
        self.enableButtons(False)
        self.errorLabel.clear()
        (fsSource, irodsDestIdx, irodsDestPath) = self.getPathsFromTrees()
        if fsSource == None or irodsDestPath == None: 
            self.errorLabel.setText(
                    "ERROR Up/Download: Please select source and destination.")
            self.enableButtons(True)
            return
        if not self.ic.session.collections.exists(irodsDestPath):
            self.errorLabel.setText(
                    "ERROR UPLOAD: iRODS destination is file, must be collection.")
            self.enableButtons(True)
            return
        destColl = self.ic.session.collections.get(irodsDestPath)
        #if os.path.isdir(fsSource):
        self.uploadWindow = dataTransfer(self.ic, True, fsSource, destColl, 
                                                irodsDestIdx, self.getResource())
        self.uploadWindow.finished.connect(self.finishedUpDownload)


    def finishedUpDownload(self, succes, irodsIdx):
        #Refreshes iRODS sub tree ad irodsIdx (set "None" if to skip)
        #Saves upload parameters if check box is set
        if succes == True:
            if irodsIdx != None:
                self.irodsmodel.refreshSubTree(irodsIdx)
            if self.saveSettings.isChecked():
                print("FINISH UPLOAD/DOWNLOAD: saving ui parameters.")
                self.saveUIset()
            self.errorLabel.setText("INFO UPLOAD/DOWLOAD: completed.")
        self.uploadWindow = None # Release
        self.enableButtons(True)


    def download(self):
        self.enableButtons(False)
        self.errorLabel.clear()
        (fsDest, irodsSourceIdx, irodsSourcePath) = self.getPathsFromTrees()
        if fsDest == None or irodsSourcePath == None:
            self.errorLabel.setText(
                    "ERROR Up/Download: Please select source and destination.")
            self.enableButtons(True)
            return
        if os.path.isfile(fsDest):
            self.errorLabel.setText(
                    "ERROR DOWNLOAD: Local Destination is file, must be folder.")
            self.enableButtons(True)
            return
        # File           
        if self.ic.session.data_objects.exists(irodsSourcePath):
            irodsItem = self.ic.session.data_objects.get(irodsSourcePath)
        else:
            irodsItem = self.ic.session.collections.get(irodsSourcePath)
        self.uploadWindow = dataTransfer(self.ic, False, fsDest, irodsItem)
        self.uploadWindow.finished.connect(self.finishedUpDownload)


    # Helpers to check file paths before upload
    def getPathsFromTrees(self):
        source = self.dirmodel.get_checked()
        if source == None:
            return (None, None, None)     
        destIdx, destPath = self.irodsmodel.get_checked()
        if destIdx == None or os.path.isfile(destPath):
            return (None, None, None)     
        return (source, destIdx, destPath)
