# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:\irods-clientsb\gui\ui_files\tabBrowser.ui'
#
# Created by: PyQt5 UI code generator 5.15.4
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from os.path import dirname, join, pardir, realpath
import sys
from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_tabBrowser(object):
    def setupUi(self, tabBrowser):
        tabBrowser.setObjectName("tabBrowser")
        tabBrowser.resize(1278, 781)
        tabBrowser.setStyleSheet("QWidget\n"
"{\n"
"    color: rgb(86, 184, 139);\n"
"    background-color: rgb(54, 54, 54);\n"
"    selection-background-color: rgb(58, 152, 112);\n"
"}\n"
"\n"
"QTabWidget\n"
"{\n"
"background-color: rgb(85, 87, 83);\n"
"}\n"
"\n"
"QTableWidget\n"
"{\n"
"background-color: rgb(85, 87, 83);\n"
"selection-background-color: rgb(58, 152, 112);\n"
"}\n"
"\n"
"QLabel#errorLabel\n"
"{\n"
"color: rgb(217, 174, 23);\n"
"}\n"
"\n"
"QLineEdit#inputPath\n"
"{\n"
"background-color: rgb(85, 87, 83);\n"
"}\n"
"\n"
"QPushButton#dataDeleteButton\n"
"{\n"
"background-color: rgb(164, 0, 0);\n"
"color: rgb(46, 52, 54);\n"
"}")
        self.verticalLayout = QtWidgets.QVBoxLayout(tabBrowser)
        self.verticalLayout.setObjectName("verticalLayout")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.inputPath = QtWidgets.QLineEdit(tabBrowser)
        font = QtGui.QFont()
        font.setPointSize(16)
        self.inputPath.setFont(font)
        self.inputPath.setStyleSheet("")
        self.inputPath.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.inputPath.setPlaceholderText("")
        self.inputPath.setClearButtonEnabled(True)
        self.inputPath.setObjectName("inputPath")
        self.gridLayout.addWidget(self.inputPath, 0, 1, 1, 1)
        self.createCollButton = QtWidgets.QPushButton(tabBrowser)
        self.createCollButton.setObjectName("createCollButton")
        self.gridLayout.addWidget(self.createCollButton, 0, 5, 1, 1)
        self.label_2 = QtWidgets.QLabel(tabBrowser)
        font = QtGui.QFont()
        font.setPointSize(16)
        font.setBold(True)
        font.setWeight(75)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 0, 0, 1, 1)
        self.homeButton = QtWidgets.QPushButton(tabBrowser)
        self.homeButton.setText("")
        icon = QtGui.QIcon()
        if getattr(sys, 'frozen', False):
                icon.addPixmap(QtGui.QPixmap(join(dirname(realpath(sys.argv[0])), pardir) + "/icons/home.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        else:
                icon.addPixmap(QtGui.QPixmap(dirname(realpath(sys.argv[0])) + "/icons/home.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.homeButton.setIcon(icon)
        self.homeButton.setObjectName("homeButton")
        self.gridLayout.addWidget(self.homeButton, 0, 2, 1, 1)
        self.UploadButton = QtWidgets.QPushButton(tabBrowser)
        self.UploadButton.setObjectName("UploadButton")
        self.gridLayout.addWidget(self.UploadButton, 1, 5, 1, 1)
        self.DownloadButton = QtWidgets.QPushButton(tabBrowser)
        self.DownloadButton.setObjectName("DownloadButton")
        self.gridLayout.addWidget(self.DownloadButton, 1, 2, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        spacerItem = QtWidgets.QSpacerItem(20, 10, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.verticalLayout.addItem(spacerItem)
        self.collTable = QtWidgets.QTableWidget(tabBrowser)
        self.collTable.setMinimumSize(QtCore.QSize(0, 250))
        self.collTable.setStyleSheet("")
        self.collTable.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.collTable.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.collTable.setAlternatingRowColors(False)
        self.collTable.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.collTable.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.collTable.setObjectName("collTable")
        self.collTable.setColumnCount(5)
        self.collTable.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        item.setText("Path")
        self.collTable.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.collTable.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.collTable.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.collTable.setHorizontalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        self.collTable.setHorizontalHeaderItem(4, item)
        self.verticalLayout.addWidget(self.collTable)
        spacerItem1 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.verticalLayout.addItem(spacerItem1)
        self.viewTabs = QtWidgets.QTabWidget(tabBrowser)
        self.viewTabs.setStyleSheet("")
        self.viewTabs.setObjectName("viewTabs")
        self.preview = QtWidgets.QWidget()
        font = QtGui.QFont()
        font.setBold(False)
        font.setWeight(50)
        self.preview.setFont(font)
        self.preview.setAutoFillBackground(False)
        self.preview.setObjectName("preview")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.preview)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.previewBrowser = QtWidgets.QTextBrowser(self.preview)
        self.previewBrowser.setObjectName("previewBrowser")
        self.verticalLayout_2.addWidget(self.previewBrowser)
        self.viewTabs.addTab(self.preview, "")
        self.metadata = QtWidgets.QWidget()
        self.metadata.setObjectName("metadata")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.metadata)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.metadataTable = QtWidgets.QTableWidget(self.metadata)
        self.metadataTable.setMinimumSize(QtCore.QSize(600, 300))
        self.metadataTable.setStyleSheet("")
        self.metadataTable.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.metadataTable.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.metadataTable.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.metadataTable.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.metadataTable.setObjectName("metadataTable")
        self.metadataTable.setColumnCount(3)
        self.metadataTable.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.metadataTable.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.metadataTable.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.metadataTable.setHorizontalHeaderItem(2, item)
        self.horizontalLayout.addWidget(self.metadataTable)
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setObjectName("gridLayout_2")
        spacerItem2 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_2.addItem(spacerItem2, 9, 0, 1, 1)
        self.metaUnitsField = QtWidgets.QLineEdit(self.metadata)
        self.metaUnitsField.setObjectName("metaUnitsField")
        self.gridLayout_2.addWidget(self.metaUnitsField, 6, 2, 1, 1)
        self.label_6 = QtWidgets.QLabel(self.metadata)
        self.label_6.setObjectName("label_6")
        self.gridLayout_2.addWidget(self.label_6, 4, 1, 1, 1)
        self.metaUpdateButton = QtWidgets.QPushButton(self.metadata)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.metaUpdateButton.setFont(font)
        self.metaUpdateButton.setObjectName("metaUpdateButton")
        self.gridLayout_2.addWidget(self.metaUpdateButton, 7, 1, 1, 1)
        self.metaKeyField = QtWidgets.QLineEdit(self.metadata)
        self.metaKeyField.setObjectName("metaKeyField")
        self.gridLayout_2.addWidget(self.metaKeyField, 6, 0, 1, 1)
        self.metaAddButton = QtWidgets.QPushButton(self.metadata)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.metaAddButton.setFont(font)
        self.metaAddButton.setObjectName("metaAddButton")
        self.gridLayout_2.addWidget(self.metaAddButton, 7, 0, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.metadata)
        font = QtGui.QFont()
        font.setPointSize(20)
        font.setBold(True)
        font.setWeight(75)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.gridLayout_2.addWidget(self.label_3, 0, 0, 1, 1)
        self.label_5 = QtWidgets.QLabel(self.metadata)
        self.label_5.setObjectName("label_5")
        self.gridLayout_2.addWidget(self.label_5, 4, 2, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.metadata)
        self.label_4.setObjectName("label_4")
        self.gridLayout_2.addWidget(self.label_4, 4, 0, 1, 1)
        self.metaDeleteButton = QtWidgets.QPushButton(self.metadata)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.metaDeleteButton.setFont(font)
        self.metaDeleteButton.setObjectName("metaDeleteButton")
        self.gridLayout_2.addWidget(self.metaDeleteButton, 7, 2, 1, 1)
        self.metaValueField = QtWidgets.QLineEdit(self.metadata)
        self.metaValueField.setObjectName("metaValueField")
        self.gridLayout_2.addWidget(self.metaValueField, 6, 1, 1, 1)
        self.horizontalLayout.addLayout(self.gridLayout_2)
        self.viewTabs.addTab(self.metadata, "")
        self.accession = QtWidgets.QWidget()
        self.accession.setObjectName("accession")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.accession)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.aclTable = QtWidgets.QTableWidget(self.accession)
        self.aclTable.setMinimumSize(QtCore.QSize(600, 0))
        self.aclTable.setStyleSheet("")
        self.aclTable.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.aclTable.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.aclTable.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.aclTable.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.aclTable.setObjectName("aclTable")
        self.aclTable.setColumnCount(3)
        self.aclTable.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.aclTable.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.aclTable.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.aclTable.setHorizontalHeaderItem(2, item)
        self.horizontalLayout_2.addWidget(self.aclTable)
        self.gridLayout_4 = QtWidgets.QGridLayout()
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.label_10 = QtWidgets.QLabel(self.accession)
        self.label_10.setObjectName("label_10")
        self.gridLayout_4.addWidget(self.label_10, 4, 2, 1, 1)
        self.aclAddButton = QtWidgets.QPushButton(self.accession)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.aclAddButton.setFont(font)
        self.aclAddButton.setObjectName("aclAddButton")
        self.gridLayout_4.addWidget(self.aclAddButton, 7, 3, 1, 1)
        self.label_11 = QtWidgets.QLabel(self.accession)
        font = QtGui.QFont()
        font.setPointSize(20)
        font.setBold(True)
        font.setWeight(75)
        self.label_11.setFont(font)
        self.label_11.setObjectName("label_11")
        self.gridLayout_4.addWidget(self.label_11, 0, 0, 1, 1)
        self.label_7 = QtWidgets.QLabel(self.accession)
        self.label_7.setObjectName("label_7")
        self.gridLayout_4.addWidget(self.label_7, 4, 3, 1, 1)
        self.aclBox = QtWidgets.QComboBox(self.accession)
        self.aclBox.setObjectName("aclBox")
        self.aclBox.addItem("")
        self.aclBox.addItem("")
        self.aclBox.addItem("")
        self.aclBox.addItem("")
        self.aclBox.addItem("")
        self.gridLayout_4.addWidget(self.aclBox, 6, 2, 1, 1)
        self.aclUserField = QtWidgets.QLineEdit(self.accession)
        self.aclUserField.setObjectName("aclUserField")
        self.gridLayout_4.addWidget(self.aclUserField, 6, 0, 1, 1)
        self.label_13 = QtWidgets.QLabel(self.accession)
        self.label_13.setObjectName("label_13")
        self.gridLayout_4.addWidget(self.label_13, 4, 0, 1, 1)
        self.recurseBox = QtWidgets.QComboBox(self.accession)
        self.recurseBox.setObjectName("recurseBox")
        self.recurseBox.addItem("")
        self.recurseBox.addItem("")
        self.gridLayout_4.addWidget(self.recurseBox, 6, 3, 1, 1)
        spacerItem3 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_4.addItem(spacerItem3, 8, 0, 1, 1)
        self.aclZoneField = QtWidgets.QLineEdit(self.accession)
        self.aclZoneField.setObjectName("aclZoneField")
        self.gridLayout_4.addWidget(self.aclZoneField, 6, 1, 1, 1)
        self.label_8 = QtWidgets.QLabel(self.accession)
        self.label_8.setObjectName("label_8")
        self.gridLayout_4.addWidget(self.label_8, 4, 1, 1, 1)
        self.horizontalLayout_2.addLayout(self.gridLayout_4)
        self.viewTabs.addTab(self.accession, "")
        self.resources = QtWidgets.QWidget()
        self.resources.setObjectName("resources")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.resources)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.resourceTable = QtWidgets.QTableWidget(self.resources)
        self.resourceTable.setStyleSheet("")
        self.resourceTable.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.resourceTable.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.resourceTable.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.resourceTable.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.resourceTable.setObjectName("resourceTable")
        self.resourceTable.setColumnCount(2)
        self.resourceTable.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.resourceTable.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.resourceTable.setHorizontalHeaderItem(1, item)
        self.horizontalLayout_3.addWidget(self.resourceTable)
        self.viewTabs.addTab(self.resources, "")
        self.delete_2 = QtWidgets.QWidget()
        self.delete_2.setObjectName("delete_2")
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout(self.delete_2)
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.deleteSelectionBrowser = QtWidgets.QTextBrowser(self.delete_2)
        self.deleteSelectionBrowser.setObjectName("deleteSelectionBrowser")
        self.horizontalLayout_6.addWidget(self.deleteSelectionBrowser)
        self.verticalLayout_5 = QtWidgets.QVBoxLayout()
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.loadDeleteSelectionButton = QtWidgets.QPushButton(self.delete_2)
        self.loadDeleteSelectionButton.setObjectName("loadDeleteSelectionButton")
        self.verticalLayout_5.addWidget(self.loadDeleteSelectionButton)
        spacerItem4 = QtWidgets.QSpacerItem(20, 30, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.verticalLayout_5.addItem(spacerItem4)
        self.dataDeleteButton = QtWidgets.QPushButton(self.delete_2)
        font = QtGui.QFont()
        font.setPointSize(13)
        font.setBold(True)
        font.setWeight(75)
        self.dataDeleteButton.setFont(font)
        self.dataDeleteButton.setStyleSheet("")
        self.dataDeleteButton.setObjectName("dataDeleteButton")
        self.verticalLayout_5.addWidget(self.dataDeleteButton)
        self.horizontalLayout_6.addLayout(self.verticalLayout_5)
        self.viewTabs.addTab(self.delete_2, "")
        self.verticalLayout.addWidget(self.viewTabs)
        self.errorLabel = QtWidgets.QLabel(tabBrowser)
        self.errorLabel.setStyleSheet("")
        self.errorLabel.setText("")
        self.errorLabel.setObjectName("errorLabel")
        self.verticalLayout.addWidget(self.errorLabel)

        self.retranslateUi(tabBrowser)
        self.viewTabs.setCurrentIndex(3)
        QtCore.QMetaObject.connectSlotsByName(tabBrowser)

    def retranslateUi(self, tabBrowser):
        _translate = QtCore.QCoreApplication.translate
        tabBrowser.setWindowTitle(_translate("tabBrowser", "Form"))
        self.inputPath.setText(_translate("tabBrowser", "/zoneName/home/user"))
        self.createCollButton.setText(_translate("tabBrowser", "Create Collection"))
        self.label_2.setText(_translate("tabBrowser", "iRODS path: "))
        self.UploadButton.setText(_translate("tabBrowser", "File Upload"))
        self.DownloadButton.setText(_translate("tabBrowser", "File Download"))
        item = self.collTable.horizontalHeaderItem(1)
        item.setText(_translate("tabBrowser", "Name"))
        item = self.collTable.horizontalHeaderItem(2)
        item.setText(_translate("tabBrowser", "Size"))
        item = self.collTable.horizontalHeaderItem(3)
        item.setText(_translate("tabBrowser", "Checksum"))
        item = self.collTable.horizontalHeaderItem(4)
        item.setText(_translate("tabBrowser", "Last modified"))
        self.viewTabs.setTabText(self.viewTabs.indexOf(self.preview), _translate("tabBrowser", "Preview"))
        item = self.metadataTable.horizontalHeaderItem(0)
        item.setText(_translate("tabBrowser", "Key"))
        item = self.metadataTable.horizontalHeaderItem(1)
        item.setText(_translate("tabBrowser", "Value"))
        item = self.metadataTable.horizontalHeaderItem(2)
        item.setText(_translate("tabBrowser", "Units"))
        self.label_6.setText(_translate("tabBrowser", "Value"))
        self.metaUpdateButton.setText(_translate("tabBrowser", "Update"))
        self.metaAddButton.setText(_translate("tabBrowser", "Add"))
        self.label_3.setText(_translate("tabBrowser", "Edit"))
        self.label_5.setText(_translate("tabBrowser", "Units"))
        self.label_4.setText(_translate("tabBrowser", "Key"))
        self.metaDeleteButton.setText(_translate("tabBrowser", "Delete"))
        self.viewTabs.setTabText(self.viewTabs.indexOf(self.metadata), _translate("tabBrowser", "Metadata"))
        item = self.aclTable.horizontalHeaderItem(0)
        item.setText(_translate("tabBrowser", "User name"))
        item = self.aclTable.horizontalHeaderItem(1)
        item.setText(_translate("tabBrowser", "Zone"))
        item = self.aclTable.horizontalHeaderItem(2)
        item.setText(_translate("tabBrowser", "Permission type"))
        self.label_10.setText(_translate("tabBrowser", "Permission type"))
        self.aclAddButton.setText(_translate("tabBrowser", "Add"))
        self.label_11.setText(_translate("tabBrowser", "Edit"))
        self.label_7.setText(_translate("tabBrowser", "Recursive"))
        self.aclBox.setItemText(0, _translate("tabBrowser", "----"))
        self.aclBox.setItemText(1, _translate("tabBrowser", "null"))
        self.aclBox.setItemText(2, _translate("tabBrowser", "read"))
        self.aclBox.setItemText(3, _translate("tabBrowser", "write"))
        self.aclBox.setItemText(4, _translate("tabBrowser", "own"))
        self.label_13.setText(_translate("tabBrowser", "User name"))
        self.recurseBox.setItemText(0, _translate("tabBrowser", "False"))
        self.recurseBox.setItemText(1, _translate("tabBrowser", "True"))
        self.label_8.setText(_translate("tabBrowser", "UserZone"))
        self.viewTabs.setTabText(self.viewTabs.indexOf(self.accession), _translate("tabBrowser", "Permissions"))
        item = self.resourceTable.horizontalHeaderItem(0)
        item.setText(_translate("tabBrowser", "Resource name"))
        self.viewTabs.setTabText(self.viewTabs.indexOf(self.resources), _translate("tabBrowser", "Resources"))
        self.loadDeleteSelectionButton.setText(_translate("tabBrowser", "Load"))
        self.dataDeleteButton.setText(_translate("tabBrowser", "Delete"))
        self.viewTabs.setTabText(self.viewTabs.indexOf(self.delete_2), _translate("tabBrowser", "Delete"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    tabBrowser = QtWidgets.QWidget()
    ui = Ui_tabBrowser()
    ui.setupUi(tabBrowser)
    tabBrowser.show()
    sys.exit(app.exec_())
