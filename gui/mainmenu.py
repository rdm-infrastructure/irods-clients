from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QDialog, QFileDialog, QApplication, QMainWindow, QMessageBox, QPushButton, QWidget
from PyQt5.uic import loadUi
from PyQt5 import QtCore
from PyQt5 import QtGui


from gui.popupWidgets import irodsCreateCollection
from gui.irodsBrowser import irodsBrowser
from gui.elabUpload import elabUpload
from gui.irodsSearch import irodsSearch
from gui.irodsUpDownload import irodsUpDownload
from gui.irodsDataCompression import irodsDataCompression
from gui.irodsInfo import irodsInfo
from gui.irodsCreateTicket import irodsCreateTicket
from gui.irodsTicketLogin import irodsTicketLogin
from utils.utils import saveIenv

import sys
from gui.ui_files.MainMenu import Ui_MainWindow


class mainmenu(QMainWindow, Ui_MainWindow):
    def __init__(self, widget, ic, ienv):
        super(mainmenu, self).__init__()
        super(mainmenu, self).setupUi(self)
        #loadUi("gui/ui_files/MainMenu.ui", self)

        self.ic = ic
        self.widget = widget #stackedWidget
        self.ienv = ienv

        # Menu actions
        self.actionExit.triggered.connect(self.programExit)
        self.actionCloseSession.triggered.connect(self.newSession)

        if not ienv or not ic:
            self.actionSearch.setEnabled(False)
            self.actionSaveConfig.setEnabled(False)
            self.ticketAccessTab = irodsTicketLogin()
            #ticketAccessWidget = loadUi("gui/ui_files/tabTicketAccess.ui")
            self.tabWidget.addTab(self.ticketAccessTab, "Ticket Access")

        else:
            self.actionSearch.triggered.connect(self.search)
            self.actionSaveConfig.triggered.connect(self.saveConfig)
            #self.actionExportMetadata.triggered.connect(self.exportMeta)

            #needed for Search
            self.irodsBrowser = irodsBrowser(ic)
            self.tabWidget.addTab(self.irodsBrowser, "Browser")
            if ("ui_tabs" in ienv) and (ienv["ui_tabs"] != ""): 
                # Setup up/download tab, index 1
                if ("tabUpDownload" in ienv["ui_tabs"]):
                    self.updownload = irodsUpDownload(ic, self.ienv)
                    self.tabWidget.addTab(self.updownload, "Up and Download")
    
                # Elabjournal tab, index 2
                if ("tabELNData" in ienv["ui_tabs"]):
                    self.elnTab = elabUpload(ic)
                    #elabUploadWidget = loadUi("gui/ui_files/tabELNData.ui")
                    self.tabWidget.addTab(self.elnTab, "ELN Data upload")
    
                # Data compression tab, index 3
                if ("tabDataCompression" in ienv["ui_tabs"]):
                    self.compressionTab = irodsDataCompression(ic, self.ienv)
                    #dataCompressWidget = loadUi("gui/ui_files/tabDataCompression.ui")
                    self.tabWidget.addTab(self.compressionTab, "Compress/bundle data")
    
                # Grant access by tickets, index 4
                if ("tabCreateTicket" in ienv["ui_tabs"]):
                    self.createTicket = irodsCreateTicket(ic, self.ienv)
                    #createTicketWidget = loadUi("gui/ui_files/tabTicketCreate.ui")
                    self.tabWidget.addTab(self.createTicket, "Create access tokens")
    
            #general info
            self.irodsInfo = irodsInfo(ic)
            #self.infoWidget = loadUi("gui/ui_files/tabInfo.ui")
            self.tabWidget.addTab(self.irodsInfo, "Info")
            self.tabWidget.setCurrentIndex(0)

    #connect functions
    def programExit(self):
        quit_msg = "Are you sure you want to exit the program?"
        reply = QMessageBox.question(self, 'Message', quit_msg, QMessageBox.Yes, QMessageBox.No)
        if reply == QMessageBox.Yes:
            if self.ic:
                self.ic.session.cleanup()
            elif self.ticketAccessTab.ic:
                self.ticketAccessTab.ic.closeSession()
            sys.exit()
        else:
            pass


    def newSession(self):
        quit_msg = "Are you sure you want to disconnect?"
        reply = QMessageBox.question(self, 'Message', quit_msg, QMessageBox.Yes, QMessageBox.No)
        if reply == QMessageBox.Yes:
            if self.ic:
                self.ic.session.cleanup()
            elif self.ticketAccessTab.ic:
                self.ticketAccessTab.ic.closeSession()
            currentWidget = self.widget.currentWidget()
            self.widget.setCurrentIndex(self.widget.currentIndex()-1)
            self.widget.removeWidget(currentWidget)
            currentWidget = self.widget.currentWidget()
            currentWidget.init_envbox()
        else:
            pass

    def search(self):
        search = irodsSearch(self.ic, self.irodsBrowser.collTable)
        search.exec_()


    def saveConfig(self):
        path = saveIenv(self.ienv)
        self.globalErrorLabel.setText("Environment saved to: "+path)

    def exportMeta(self):
        print("TODO: Metadata export")

